require 'yaml'
require 'awesome_print'

def indent_spacer(indent)
  spacer = ''
  indent.times { spacer += '  ' }
  spacer
end

def nest_diff(config1, config2, indent)
  config1.each do |key, value|
    print "#{indent_spacer(indent)}#{key}-> "

    if !config2[key]
      puts " x"
    else
      puts " o"
      if value.is_a?(Hash)
        indent += 1
        nest_diff(config1[key], config2[key], indent)
      end
    end

  end
end


dir_path = ARGV[0]

files = Dir.glob("#{dir_path}/**/ja.yml")

files.each do |file|
  ja_locale_file = file
  en_locale_file = file.gsub('ja.yml', 'en.yml')

  puts ja_locale_file

  ja_locale_config = YAML.load_file(ja_locale_file)
  if File.exist?(en_locale_file)
    en_locale_config = YAML.load_file(en_locale_file)
  else
    puts " Not found en.yml"
    next
  end

  indent = 1
  if en_locale_config['en']
    nest_diff(ja_locale_config['ja'], en_locale_config['en'], indent)
  else
    puts " No data en.yml"
  end
end
